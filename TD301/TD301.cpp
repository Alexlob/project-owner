#include "TD301.h"

///Constrcuteur (par d�faut)
Point::Point() : m_CoX(0.0), m_CoY(0.0)
{

}

///Destructeur
Point::~Point()
{

}

///Getters
float Point :: getCoX () const
{
    return m_CoX;
}

float Point :: getCoY () const
{
    return m_CoY;
}
///Setters
void Point:: setCoX (float decale)
{
    m_CoX = decale;
}
void Point:: setCoY (float decaleBis)
{
    m_CoY = decaleBis;
}

///M�thodes
void Point:: monter ()
{

    float decaleBis;
    decaleBis = getCoY() + 0.1;
    setCoY(decaleBis);
}
void Point:: descendre ()
{

    float decaleBis;
    decaleBis = getCoY() - 0.1;
    setCoY(decaleBis);
}
void Point:: allerAGauche ()
{

    float decale;
    decale = getCoX() - 0.1;
    setCoX(decale);
}
void Point:: allerADroite ()
{

    float decale;
    decale = getCoX() + 0.1;
    setCoX(decale);
}
