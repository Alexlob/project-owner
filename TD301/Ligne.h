#ifndef LIGNE_H_INCLUDED
#define LIGNE_H_INCLUDED

#include "TD301.h"
#include <iostream>

class Ligne
{
private :
    Point pointUn; ///On cr�e deux points de type Point
    Point pointDeux;

public :
    Ligne(); ///Constructeur par d�faut
    ~Ligne(); ///Destructeur
    ///Setters
    void setPointUn (float _decale, float _decaleBis);
    void setPointDeux (float _decale, float _decaleBis);
    ///M�thodes
    void action(); ///On commande les actions � effectuer sur les points ici (et non pas dans la classe Point)
    void affiche();

};


#endif // LIGNE_H_INCLUDED
