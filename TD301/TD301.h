#ifndef TD301_H_INCLUDED
#define TD301_H_INCLUDED

#include <iostream>
#include "TD301.h"


class Point
{
private :
    float m_CoX;
    float m_CoY;
public :

    ///Constructeur par d�faut
    Point ();
    ///Destructeur
    ~Point ();
    ///M�thodes
    void monter (); ///Les op�rations "monter" et "descendre" se font selon l'axe des Y
    void descendre ();
    void allerAGauche (); ///Les op�rations "allerAGauche" et "allerADroite" se font selon l'axe des X
    void allerADroite ();
    ///Accesseurs
    float getCoX () const; ///Getters
    float getCoY () const;
    void setCoX (float decale); ///On utilise des setters puisqu'on veut modifier la valeur des attributs
    void setCoY (float decaleBis);
};


#endif // TD301_H_INCLUDED
