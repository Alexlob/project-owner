#include "Ligne.h"
#include "TD301.h"

///Constructeur (par d�faut)
Ligne::Ligne()
{

}
///Destructeur
Ligne::~Ligne()
{

}
///Setters
void Ligne :: setPointUn (float _decale, float _decaleBis)
{
    pointUn.setCoX (_decale);
    pointUn.setCoY(_decaleBis);
}

void Ligne :: setPointDeux (float _decale, float _decaleBis)
{
    pointDeux.setCoX (_decale);
    pointDeux.setCoY(_decaleBis);
}

///M�thodes
void Ligne :: action ()
{
    pointUn.monter();
    pointUn.allerAGauche();
    pointDeux.descendre();
    pointDeux.allerADroite();
}

void Ligne :: affiche ()
{
    action ();
    std::cout << "POINT 1" << std::endl;
    std::cout << "X:" << pointUn.getCoX() << std::endl;
    std::cout << "Y:" << pointUn.getCoY() << std::endl;
    std::cout << "POINT 2" << std::endl;
    std::cout << "X:" << pointDeux.getCoX() << std::endl;
    std::cout << "Y:" << pointDeux.getCoY() << std::endl;
}











